import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  AsyncStorage,
  TouchableOpacity,
  StatusBar, Alert,
  ImageBackground,
  BackHandler,Linking,ScrollView
} from 'react-native';
import {
  Container,
  Content,
  Spinner,
  Footer,Right,
  FooterTab,Icon,Header,Left,Body,Card,CardItem,Title,Item,Input
} from 'native-base';
import CountDown from 'react-native-countdown-component';
//import CountDown to show the timer
import moment from 'moment';
//import moment to help you play with date and time
import Grid from 'react-native-grid-component';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
import Imagel from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import {AdMobInterstitial} from 'react-native-admob';
export default class Bidandwin extends Component {
  constructor(props) {
    super(props);
    //initialize the counter duration
    this.state = {
      totalDuration: '',
      data:'',
      time:'2018-11-23 12:00:00',
      products:[],
      user_id:'',
      timmer:"",
      items:[],
      bitval:'',
      freebids:'',
      paidbids:'',
    };
  }
 componentWillMount(){
  // AdMobInterstitial.setAdUnitID('ca-app-pub-3493193453765061/5642304535');
  // AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
      this.getproducts()
      AsyncStorage.getItem('user')
      .then((value) => {
          if (value != null || value != undefined) {
              const values = JSON.parse(value)
              console.log('user available' + values.userid)
              console.log(values)
              this.setState({user_id:values.userid})
              this.viewuser(this.state.user_id)
          }
        })
 }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    var that = this;
    
  }
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });
   //modal for edit profile------------
   modalDidOpen1 = () => console.log("Modal did open");
   modalDidClose1 = () => {
     this.setState({ open1: false });
   };
   openModal1 = () => this.setState({ open1: true });
   closeModal1 = () => this.setState({ open1: false });
  
   //-------------------------------
   getproducts(){
     this.setState({open:true})
     this.setState({products:[]})
    fetch(urldetails.base_url+urldetails.url["productview"],{
      method:"GET",
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    if(jsondata.status=="success"){
    this.setState({products:jsondata.Products,open:false})
    console.log(this.state.products)
    
    }
    else{
      this.setState({open:true})
    }
   
  })
   }
   viewuser(user_id){
    var formData= new FormData();
    formData.append('user_id',user_id);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["viewuser"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      this.setState({open:false,freebids:jsondata.account.acc_freebids,paidbids:jsondata.account.acc_paidbids})
      console.log(this.state.paidbids)
    }
    else{
      this.setState({open:false}) 
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.viewuser(this.state.user_id)},
      ],
      { cancelable: false }
    )
    });
  }
   bid(){
    // Alert.alert(this.state.bitval) 
     if(this.state.bitval.length ==0){
     
      Alert.alert('Bid value can not be blank!') 
     }
     else if(this.state.bitval<0){
      Alert.alert('Bid value must be greater than 0') 
     }
     else {
     var formData= new FormData();
     formData.append('user_id',this.state.user_id);
     formData.append('bid_value',this.state.bitval);
     formData.append('product_id',this.state.pid);
     console.log(formData)
     fetch(urldetails.base_url+urldetails.url["dobids"],{
       method:"POST",
       body:formData
     })
     .then((response)=>response.json())
   .then((jsondata)=>{
     console.log(jsondata)
     if(jsondata.status=="success"){
       this.setState({open1:false,bitval:""})
      Alert.alert("",jsondata.message)
     }
     else if(jsondata.status=="failed"){
      this.setState({ open1: false })
      Alert.alert("",jsondata.message)
    }
   }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.bid()},
      ],
      { cancelable: false }
    )
    });
  }
   }
  
open(pid,bidprice){
  if(this.state.freebids+this.state.paidbids < bidprice)
  {
    alert("You have insufficient bids for this product")
  }
  else{
    this.setState({open1:true,pid:pid})
  }
}

  render() {
    return (
      <Container>
      <Header style={{ backgroundColor: '#047BD5' }}>
        <Left>
        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
          <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
        </TouchableOpacity>
        </Left>
        <Body><Title>Bid and Win</Title></Body>
        </Header>
        <StatusBar backgroundColor="#1565C0"/>
        <Content style={{backgroundColor:'#f5f5f5'}}>
        {
          this.state.products.map((cat, i) => {
            return(
              <View style={{alignItems:'center',justifyContent:'center'}} key={i}>
                 { Object.values(cat)[0].length>0?
              <Card style={{backgroundColor:'#fff',width:screenwidth-15}} >
              <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate(("Product"),{type:Object.keys(cat),products:this.state.products})}><Text style={{paddingLeft:10,paddingTop:10,fontSize:14,color:'#696969'}}>{Object.keys(cat)}</Text></TouchableOpacity>
              <Right style={{paddingRight:10,paddingTop:10}}><TouchableOpacity onPress={()=>this.props.navigation.navigate(("Product"),{type:Object.keys(cat),products:this.state.products})}><Text style={{fontSize:13,color:'#047BD5'}}>MORE</Text></TouchableOpacity></Right>
              </View>
            <ScrollView horizontal="true" style={{backgroundColor:'#fff',padding:5}}>
            {Object.values(cat)[0].map((item,j)=>{
            return(
              <Card  style={{marginLeft:5,width:screenwidth/2.7,borderWidth:0.3,backgroundColor:'white',alignItems:"center",
              alignContent:"center",flexDirection:'column',flex:1,margin:1}} key={j}>
                <Text style={{color:'#000',paddingTop:3}}>{item.p_name.substr(0,26)}</Text>
                <Imagel source={{uri:urldetails.image+item.p_imgpath}} indicator={ProgressBar.Pie} resizeMode="contain"
                                      indicatorProps={{ size: 80, borderWidth: 0, color: 'rgba(150, 150, 150, 1)', unfilledColor: 'rgba(200, 200, 200, 0.2)' }}
              style={{width:screenwidth/3.4,height:screenHeight/6}}  />
              {item.timeduration>0?
               <CountDown until={item.timeduration} onFinish={() =>this.componentWillMount()}
               digitBgColor="#fff" timetoShow={('D','H', 'M', 'S')} timeTxtColor="red" size={12} />
               :<Text style={{color:'red'}}>{item.p_soldto}</Text>}
                <View style={{flexDirection:'row',paddingTop:3}}><Text style={{color:'#000'}}>Bid Price:</Text><Text style={{paddingLeft:2}}>{item.p_bid_price}</Text></View>
                {item.timeduration>0?<CardItem footer>
                  <TouchableOpacity onPress={()=>this.open(item.pid,item.p_bid_price)} style={{backgroundColor:'#DE6F10',width:screenwidth/3.5,borderRadius:3,height:30,justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:13,color:'#fff'}}>BID NOW</Text>
                  </TouchableOpacity>
                </CardItem>:<View/>}  
              <View style={{paddingBottom:10}}>
              <View style={{flexDirection:'row'}}><Text style={{color:'#008000',fontSize:13}}>Product Price:</Text><Text style={{paddingLeft:2,color:'#008000'}}>₹{item.p_price}</Text></View>
               
              </View> 
          </Card>
              )
            })
          }
          
            </ScrollView>
            
          </Card>:<View/>}
          </View>
            )
          }
          )
        }
       
      </Content>
      <Modal
        offset={0}
        closeOnTouchOutside={false} disableOnBackPress={true}
        open={this.state.open}
        modalDidOpen={this.modalDidOpen}
        modalDidClose={this.modalDidClose}
        style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
        <View style={{alignContent:'center',alignItems:'center'}}>
        <Spinner color='#DE6F10' />
          </View>
        </Modal>
      <Modal1 offset={0} open={this.state.open1} modalDidOpen={this.modalDidOpen1} 
                modalDidClose={this.modalDidClose1} style={{ alignItems: "center", }}>
                
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
                <Left></Left>
                <Text style={{ fontSize: 20, marginBottom: 10, textAlign: 'center' }}>BID AND WIN</Text>
                
                <Right/>
            </View>
        <View style={{ flexDirection:'column' ,alignItems: 'center', justifyContent: 'center' }}>
        <Item regular style={{borderColor:'black',height:40,width:screenwidth/1.5}}>
            <Input placeholder='Enter Bid' value={this.state.bitval} onChangeText={(bitval) => this.setState({ bitval: bitval })}/>
          </Item>
          <TouchableOpacity onPress={()=>this.bid()} style={{marginTop:15,height: 50,width:screenwidth/2.5,backgroundColor: "#DE6F10",borderRadius:5,
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
         >
            <Text style={{ color: "white", fontSize: 20 }}>Submit</Text>
          </TouchableOpacity>
          </View>
</Modal1>
      </Container>
    );
  }
}

