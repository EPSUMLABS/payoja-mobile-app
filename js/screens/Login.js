
import React, {Component} from 'react';
import {Platform,TouchableOpacity,StyleSheet, Text, View,Dimensions,StatusBar,AsyncStorage,BackHandler,Alert} from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label,Icon,Right,Spinner } from 'native-base';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
import { validateEmail, validatePassword } from '../config/validate';
import Modal from "react-native-simple-modal";
import FBSDK,{LoginManager,AccessToken,GraphRequest,GraphRequestManager} from 'react-native-fbsdk';
export default class Login extends Component {
  state = {
    email:'',
    password:'',
    isFocused: false,
  };
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
   componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    BackHandler.exitApp()
  };
  componentWillMount(){
    // this.setState({email:'',password:''})
    AsyncStorage.setItem('user',"");
  }
modalDidOpen = () => console.log("Modal did open.");
modalDidClose = () => {
  this.setState({ open: false });
  console.log("Modal did close.");
};
moveUp = () => this.setState({ offset: -100 });
resetPosition = () => this.setState({ offset: 0 });
openModal = () => this.setState({ open: true });
closeModal = () => this.setState({ open: false });
login(){
  if (this.state.email == "") {
    alert('Email field cannot be blank !')
  }
  else if (!validateEmail(this.state.email.trim())) {
    alert('Please enter valid email !')
  }
   else if (this.state.password == "") {
    alert('Password cannot be blank !')
  }
   else {
  this.setState({open:true})
  var formData= new FormData();
  formData.append('u_email',this.state.email);
  formData.append('u_password',this.state.password);
  console.log(formData)
  fetch(urldetails.base_url+urldetails.url["userlogin"],{
    method:"POST",
    body:formData
  })
  .then((response)=>response.json())
.then((jsondata)=>{
  console.log(jsondata)
  if(jsondata.status=="success"){
    // this.setState({email:"",password:""})
    this.setState({open:false})
    var userdetails={
      userid:jsondata.data['user_id'],
      username:jsondata.data['u_name'],
      email:jsondata.data['u_email'],
      contact:jsondata.data['u_phoneno'],
      referalid:jsondata.data['u_referalid'],
      referal:jsondata.referal
    }
    console.log(userdetails)
    AsyncStorage.setItem('user',JSON.stringify(userdetails));
    this.props.navigation.navigate("Home")
  }
  else if(jsondata.status=="failed"){
    this.setState({ open: false })
    Alert.alert('',jsondata.message)
  }
}
) .catch((error) => {
console.log(error)
Alert.alert(
  'Alert',
  "Please check your Internet Connection.",
  [
    {text: 'Retry', onPress: () =>this.login()},
  ],
  { cancelable: false }
)
});
}
}
_fbLogin(){
  this.setState({ spinner: true })
  LoginManager.logInWithReadPermissions(['public_profile','email'])
  .then((result)=>{
    if(result.isCancelled){
      alert("Login is cancelled "+result.isCancelled)
      this.setState({ spinner: false })
    }else{
        AccessToken.getCurrentAccessToken()
        .then((data)=>{
          let accessToken = data.accessToken
          this.setState({name:data.accessToken.toString()})

          const responseInfoCallback = (error,result)=>{
            if(error){
              this.setState({ spinner: false })
            }else{
              this.sendsocialinfo(result.id,result.email,result.name,"Facebook")
            }
          }
          const infoRequest = new GraphRequest('/me',{
            accessToken:accessToken,
            parameters:{
              fields:{
                string:"email,name,first_name,last_name,picture"
              }
            }
          },
          responseInfoCallback);
          new GraphRequestManager()
          .addRequest(infoRequest)
          .start()
        })

    }
  },function(error){
    alert("An error occured"+error.toString());
  }
  )
}
  render() {
    const { isFocused } = this.state;
    const labelStyle = {
      position: 'absolute',
      left: 0,
      top: !isFocused ? 5 : 0,
      fontSize: !isFocused ? 12 : 12,
      color: !isFocused ? '#818181' : '#047BD5',
    };
    return (
        <Container>
          <StatusBar backgroundColor="#1565C0"/>
         <Content style={{backgroundColor:'#047BD5'}}>
         
        <View style={{alignItems:'center',paddingTop:'20%',justifyContent:'center'}}>
           <Text style={{padding:20,fontSize:25,color:'#fff'}}>LOGIN</Text>
           <Item regular style={{width:screenwidth-50,height:50,marginTop:20,borderRadius:5,borderColor: '#fff',borderWidth:0.5}}>
              <Icon name='ios-mail' style={{color:'#fff',marginLeft:3}} />
              
              <Input placeholder='Email' placeholderTextColor='#fff'style={{color:'#fff',paddingLeft:5}}  onChangeText={(email)=>this.setState({email:email.toLowerCase()})} />
          </Item>
          <Item regular style={{width:screenwidth-50,height:50,marginTop:20,borderRadius:5,borderColor: '#fff',borderWidth:0.5}}>
           
              <Icon name='ios-lock' style={{color:'#fff',marginLeft:3}}/>
              <Input placeholder='Password'  placeholderTextColor='#fff'style={{color:'#fff',paddingLeft:5}} onChangeText={(password)=>this.setState({password:password})}  secureTextEntry={true}  autoCorrect={false} />
            
          </Item>
            <TouchableOpacity style={{width:screenwidth-40,height:screenHeight/15,marginTop:30,backgroundColor:'#fff',borderRadius:5,alignContent:'center',alignItems:'center',justifyContent:'center'}}
            onPress={()=>this.login()}>
            <Text style={{fontSize:20,color:'#DE6F10'}}>Login</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={()=>this._fbLogin()} style={{width:screenwidth-40,height:screenHeight/15,marginTop:20,backgroundColor:'#3b5998',borderRadius:5,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
            <Text style={{fontSize:20,color:'#fff'}}>Continue With Facebook</Text>
            </TouchableOpacity> */}
            <View style={{flexDirection:'row',paddingTop:15}}>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Signup")}>
        <Text style={{color:'#fff',fontSize:13}}>Create Account</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Forgotpassword")}>
        <Text style={{color:'#fff',fontSize:13,paddingLeft:'30%'}}>Forgot Password</Text>
        </TouchableOpacity>
        </View>
        </View>
        </Content>
        <Modal
            offset={0}
            closeOnTouchOutside={false} disableOnBackPress={true}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#DE6F10' />
              </View>
            </Modal>
      </Container>
    );
  }
}

