
import React, {Component} from 'react';
import {Platform,TouchableOpacity,StyleSheet, Text,Alert, View,Dimensions,StatusBar,BackHandler,AsyncStorage,Image} from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label,Icon,Right,Spinner,Left } from 'native-base';
import urldetails from '../config/endpoints.json';
import { validateEmail, validatePhone, validateName, validatePassword } from '../config/validate';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
export default class Signup extends Component {
  state={
    open:false,
    open1:false,
    name:'',
    email:'',
    contact:'',
    Password:''
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Login");
    return true;
  };
  modalDidOpen = () => console.log("Modal did open.");
modalDidClose = () => {
  this.setState({ open: false });
  console.log("Modal did close.");
};
moveUp = () => this.setState({ offset: -100 });
resetPosition = () => this.setState({ offset: 0 });
openModal = () => this.setState({ open: true });
closeModal = () => this.setState({ open: false });
modalDidOpen1 = () => console.log("Modal did open.");
modalDidClose1 = () => {
  this.setState({ open1: false });
  console.log("Modal did close.");
};
moveUp = () => this.setState({ offset: -100 });
resetPosition = () => this.setState({ offset: 0 });
openModal = () => this.setState({ open1: true });
closeModal = () => this.setState({ open1: false });
  register(){
     // this.setState({open:true})
     if (this.state.name == "") {
      alert('Please enter your Name ..')
    }
    else if (!validateName(this.state.name)) {
      alert('Name must contain alphabet only..')
    }
    else if (this.state.email == "") {
      alert('Email field cannot be blank !')
    }
    else if (!validateEmail(this.state.email.trim())) {
      alert('Please enter valid email !')
    }
    else if (this.state.contact == "") {
      alert('Phone number field cannot be blank !')
    }
    else if (!validatePhone(this.state.contact)) {
      alert('Please enter valid phone Number !')
    }
    else if (this.state.password == "") {
      alert('Password cannot be blank !')
    }
    // else if (!validatePassword(this.state.password)) {
    //   alert('Password should contain one uppercase letter,one digit and should be of 6 characters ..')
    // }
    else{
      this.setState({open:true})
    var formData= new FormData();
    formData.append('u_name',this.state.name);
    formData.append('u_email',this.state.email);
    formData.append('u_phoneno',this.state.contact);
    formData.append('u_password',this.state.Password);
    console.log(formData)
    fetch(urldetails.base_url + urldetails.url["adduser"],{
      method:"POST",
      body:formData
    }) .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)
      if(jsondata.status=="success"){
        console.log(jsondata.status)
        // Alert.alert('',jsondata.message)
        // var userdetails={
        //   userid:jsondata.userdetails['user_id'],
        //   username:jsondata.userdetails['u_name'],
        //   email:jsondata.userdetails['u_email'],
        //   contact:jsondata.userdetails['u_phoneno'],
        //   referalid:jsondata.userdetails['u_referalid'],
        //   referal:jsondata.referal
        // }
        // AsyncStorage.setItem('user',JSON.stringify(userdetails));
        this.setState({ spinner: false,open:false,open1:true,user_id:jsondata.userdetails['user_id'] })
        // this.props.navigation.navigate("Home")
      }
      else{
        console.log(jsondata.message)
        Alert.alert('',jsondata.message)
        this.setState({ open: false })
      }
    }) .catch((error) => {
      this.setState({open:false})
      console.log(error)
      Alert.alert(
        'Alert',
        "Please check your Internet Connection.",
        [
          {text: 'Retry', onPress: () =>this.register()},
        ],
        { cancelable: false }
      )
    
    }); 
  }
   
  }
  referal(){
    var formData= new FormData();
    formData.append('user_id',this.state.user_id);
    formData.append('u_referalid',this.state.referalid);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["referals"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
    
      Alert.alert(
        'Alert',
        "Congtatulations.You won Rs.5",
      )
      this.props.navigation.navigate("Login")
    }
    else{
      this.setState({open:false})
      Alert.alert(
        'Alert',
        "Try Again.",
      )
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.referal()},
      ],
      { cancelable: false }
    )
    });
  }
  render() {
    return (
        <Container>
          <StatusBar backgroundColor="#1565C0"/>
         <Content style={{backgroundColor:'#047BD5'}}>
        <View style={{alignItems:'center',paddingTop:'30%',justifyContent:'center'}}>
           <Text style={{padding:10,fontSize:25,color:'#fff'}}>SIGNUP</Text>
           <Item regular style={{width:screenwidth-50,height:50,marginTop:20,borderRadius:5,borderColor: '#fff',borderWidth:0.5}}>
              <Icon name='ios-person' style={{color:'#fff',marginLeft:3}}/>
              <Input placeholder='Name'  placeholderTextColor='#fff'style={{color:'#fff',paddingLeft:5}} onChangeText={(name)=>this.setState({name:name})}  />
           
          </Item>
          <Item regular style={{width:screenwidth-50,height:50,marginTop:20,borderRadius:5,borderColor: '#fff',borderWidth:0.5}}>
              <Icon name='ios-mail' style={{color:'#fff',marginLeft:3}}/>
              <Input placeholder='Email'  placeholderTextColor='#fff'style={{color:'#fff',paddingLeft:5}} onChangeText={(email)=>this.setState({email:email.toLowerCase()})}  />
          </Item>
          <Item regular style={{width:screenwidth-50,height:50,marginTop:20,borderRadius:5,borderColor: '#fff',borderWidth:0.5}}>
              <Icon name='ios-call' style={{color:'#fff',marginLeft:3}}/>
              <Input placeholder='Phone Number'  placeholderTextColor='#fff'style={{color:'#fff',paddingLeft:5}} onChangeText={(contact)=>this.setState({contact:contact})}  />
          </Item>
          <Item regular style={{width:screenwidth-50,height:50,marginTop:20,borderRadius:5,borderColor: '#fff',borderWidth:0.5}}>
              <Icon name='ios-lock' style={{color:'#fff',marginLeft:3}}/>
              <Input placeholder='Password'  placeholderTextColor='#fff'style={{color:'#fff',paddingLeft:5}} onChangeText={(Password)=>this.setState({Password:Password})} secureTextEntry={true} />
          </Item>
            {/* <TouchableOpacity style={{width:screenwidth-40,height:screenHeight/15,marginTop:30,borderColor:'#1976D2',borderWidth:2,borderRadius:5,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
            <Text style={{fontSize:20,color:'#1976D2'}}>Login</Text>
            </TouchableOpacity> */}
            <TouchableOpacity style={{width:screenwidth-40,height:screenHeight/15,marginTop:20,backgroundColor:'#fff',borderRadius:5,alignContent:'center',alignItems:'center',justifyContent:'center'}}
            onPress={()=>this.register()}>
            <Text style={{fontSize:20,color:'#DE6F10'}}>Signup</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{width:screenwidth-40,height:screenHeight/15,marginTop:20,alignContent:'center',alignItems:'center',justifyContent:'center'}}
            onPress={()=>this.props.navigation.navigate("Login")}>
            <Text style={{fontSize:15,color:'#fff'}}>Existing User?SIGN IN</Text>
            </TouchableOpacity>

        </View>
        </Content>
        <Modal
            offset={0}
            closeOnTouchOutside={false} disableOnBackPress={true}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#DE6F10' />
              </View>
            </Modal>
            <Modal1 offset={0} open={this.state.open1} modalDidOpen={this.modalDidOpen1} modalDidClose={this.modalDidClose1} style={{ backgroundColor: "#fff" }}>
      <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', }}>
                <Left></Left>
                <Right>
                    <TouchableOpacity style={{ backgroundColor: 'black', borderRadius: 25, width: 25, height: 25 }} onPress={() => this.props.navigation.navigate("Login")} >
                    <Text style={{ color: '#fff', fontWeight: 'bold', padding: 3, textAlign: 'center' }}>X</Text>
                    </TouchableOpacity>
                </Right>
            </View>
            <View style={{ flexDirection:'column' ,alignItems: 'center',backgroundColor:'#fff', justifyContent: 'center',marginTop:5 }}>
        <Image source={{uri:'http://www.valldaro.com/wp-content/uploads/2013/03/gift.png'}} 
        style={{width:screenwidth/3,height:screenHeight/6,marginTop:5}}/>
         <Text style={{ fontSize: 16, marginBottom: 10,color:"#047BD5", textAlign: 'center' }}>Enter your friend's referal code to receive ₹5</Text>
        <Item regular style={{borderColor:'#000'}} style={{width:screenwidth/1.5,height: 50,borderColor:'#000',borderWidth:2}}>
            <Input  onChangeText={(referalid) => this.setState({ referalid: referalid })}/>
          </Item>
          <TouchableOpacity  style={{marginTop:15,width:screenwidth/1.2,height: 50,backgroundColor: "#DE6F10",borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
          onPress={() => this.referal()}>
            <Text style={{ color: "white", fontSize: 20 }}>Submit</Text>
          </TouchableOpacity> 
    </View>
          </Modal1>
      </Container>
    );
  }
}

