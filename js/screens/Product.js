
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking,ScrollView
  } from 'react-native';
import {
    Container,
    Content,
    Spinner,
    Footer,Right,
    FooterTab,Icon,Header,Left,Body,Card,CardItem,Title,Item,Input,Label
  } from 'native-base';
  import CountDown from 'react-native-countdown-component';
//import CountDown to show the timer
import moment from 'moment';
//import moment to help you play with date and time
import GridView from 'react-native-super-grid';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
import Imagel from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import {AdMobInterstitial} from 'react-native-admob';
export default class Product  extends Component {
    state={
        type: this.props.navigation.state.params.type,
        // products: this.props.navigation.state.params.products,
        totalDuration: '',
        data:'',
        user_id:'',
        timmer:"",
        bitval:'',
        freebids:'',
        paidbids:'',
        newproducts:[],
        prname:'',
        myitem:'',
        productlist:[],
        products:[]
    }
    componentWillMount(){
  // AdMobInterstitial.setAdUnitID('ca-app-pub-3493193453765061/1647628322');
  // AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
      this.getproducts()
      AsyncStorage.getItem('user')
      .then((value) => {
          if (value != null || value != undefined) {
              const values = JSON.parse(value)
              console.log('user available' + values.userid)
              console.log(values)
              this.setState({user_id:values.userid})
              this.viewuser(values.userid)
          }
        })
      //  this.filterproduct()
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Bidandwin");
        return true;
      };
    
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        var that = this;
        
      }
      modalDidOpen = () => console.log("Modal did open.");
      modalDidClose = () => {
        this.setState({ open: false });
        console.log("Modal did close.");
      };
      moveUp = () => this.setState({ offset: -100 });
      resetPosition = () => this.setState({ offset: 0 });
      openModal = () => this.setState({ open: true });
      closeModal = () => this.setState({ open: false });
       //modal for edit profile------------
       modalDidOpen1 = () => console.log("Modal did open");
       modalDidClose1 = () => {
         this.setState({ open1: false });
       };
       openModal1 = () => this.setState({ open1: true });
       closeModal1 = () => this.setState({ open1: false });
       viewuser(user_id){
        var formData= new FormData();
        formData.append('user_id',user_id);
        console.log(formData)
        fetch(urldetails.base_url+urldetails.url["viewuser"],{
          method:"POST",
          body:formData
        })
        .then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)
        if(jsondata.status=="success"){
          this.setState({open:false,freebids:jsondata.account.acc_freebids,paidbids:jsondata.account.acc_paidbids})
          console.log(this.state.paidbids)
        }
        else{
          this.setState({open:false}) 
        }
      }).catch((error) => {
        console.log(error)
        Alert.alert(
          'Alert',
          "Please check your Internet Connection.",
          [
            {text: 'Retry', onPress: () =>this.viewuser(this.state.user_id)},
          ],
          { cancelable: false }
        )
        });
      }
      getproducts(){
        this.setState({open:true})
        this.setState({products:[]})
       fetch(urldetails.base_url+urldetails.url["productview"],{
         method:"GET",
       })
       .then((response)=>response.json())
     .then((jsondata)=>{
       if(jsondata.status=="success"){
       this.setState({products:jsondata.Products,open:false})
       console.log(this.state.products)
       var arr = []
       this.state.products.map((myitem, i) => {
           console.log("obj"+Object.keys(myitem))
           console.log(this.state.type[0])
         if (Object.keys(myitem) == this.state.type[0]) {
           arr.push(Object.values(myitem)[0])
           console.log(arr)
           this.setState({newproducts:arr[0],productlist:arr[0]})
         }
        })
       }
       else{
         this.setState({open:true})
       }
      
     })
    
      }

bid(){
    // Alert.alert(this.state.bitval) 
     if(this.state.bitval.length ==0){
     
      Alert.alert('Bid value can not be blank!') 
     }
     else if(this.state.bitval<0){
      Alert.alert('Bid value must be greater than 0') 
     }
     else {
     var formData= new FormData();
     formData.append('user_id',this.state.user_id);
     formData.append('bid_value',this.state.bitval);
     formData.append('product_id',this.state.pid);
     console.log(formData)
     fetch(urldetails.base_url+urldetails.url["dobids"],{
       method:"POST",
       body:formData
     })
     .then((response)=>response.json())
   .then((jsondata)=>{
     console.log(jsondata)
     if(jsondata.status=="success"){
       this.setState({open1:false})
      Alert.alert("",jsondata.message)
     }
     else if(jsondata.status=="failed"){
      this.setState({ open1: false })
      Alert.alert("",jsondata.message)
    }
   }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.bid()},
      ],
      { cancelable: false }
    )
    });
  }
   }
   open(pid,bidprice){
    if(this.state.freebids+this.state.paidbids < bidprice)
    {
      alert("You have insufficient bids for this product")
    }
    else{
      this.setState({open1:true,pid:pid})
    }
  }
  finditem(myitem) {
    if (myitem === '') {
      return [];
    }
    const { productlist } = this.state;
    const regex = new RegExp(`${myitem.trim()}`, 'i');
    return productlist.filter(productlist => productlist.p_name.search(regex) >= 0);
  }
  filteritem(myitem){
    const newData = this.state.productlist.filter((mydata)=>{
        const itemData = mydata.p_name.toUpperCase()
        console.log(itemData)
        const textData = myitem.toUpperCase()
        console.log(textData)
        return itemData.indexOf(textData)>-1
      });
      
      this.setState({myitem:myitem,newproducts:newData});
    
    console.log(this.state.newproducts)
                  }
    render() {
      const { myitem } = this.state;
      const productlist = this.finditem(myitem);
      const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
  // if(newproducts.length > 0){
  //   console.log(newproducts[0].p_name)
  // }
  
        return (
            <Container>
        <Header style={{ backgroundColor: '#047BD5' }}>
        <View style={{flexDirection:'row', marginTop:3,alignContent:'center',alignItems:'center'}}>
          {/* <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
          <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
          </TouchableOpacity> */}
         
            <Item regular style={{height:40,width:screenwidth-20,alignItems:'center',backgroundColor:'#fff',borderRadius:2,marginLeft:5}}>
            <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
          <Icon name='ios-menu' style={{ fontSize: 25, color: '#696969' }} />
          </TouchableOpacity>
          <Input style={{fontSize:20,color:'#000',paddingLeft:15}} placeholder='Search'placeholderTextColor='#808080' placeholderTextSize ='30px'  onChangeText={(myitem)=>this.filteritem(myitem)} autoCapitalize="none" autoCorrect={false} />
        </Item>
          </View>
        </Header>
        <StatusBar backgroundColor="#1565C0"/>
        <Content style={{backgroundColor:'#fff'}}>
        <View style={{backgroundColor:'#fff'}}>
        {/* {console.log(this.state.newproducts)} */}
          <GridView
                itemDimension={130}
                items={this.state.newproducts}
                itemsPerRow={2}
                style={styles.gridView}
                renderItem={item =>(
                    <Card  style={{marginLeft:5,width:screenwidth/2.7,backgroundColor:'white',alignItems:"center",
                    alignContent:"center",flexDirection:'column',flex:1,margin:1}} >
                      <Text style={{color:'#000',paddingTop:3}}>{item.p_name.substr(0,26)}</Text>
                      <Imagel source={{uri:urldetails.image+item.p_imgpath}} indicator={ProgressBar.Pie} resizeMode="contain"
                                            indicatorProps={{ size: 80, borderWidth: 0, color: 'rgba(150, 150, 150, 1)', unfilledColor: 'rgba(200, 200, 200, 0.2)' }}
                    style={{width:screenwidth/3.4,height:screenHeight/6}}/>
                    {item.timeduration>0?
                     <CountDown until={item.timeduration} onFinish={() =>this.componentWillMount()}
                     digitBgColor="#fff" timetoShow={('D','H', 'M', 'S')} timeTxtColor="red" size={12} />
                     :<Text style={{color:'red',paddingTop:4}}>{item.p_soldto}</Text>}
                      <View style={{flexDirection:'row',paddingTop:3}}><Text style={{color:'#000'}}>Bid Price:</Text><Text style={{paddingLeft:2}}>{item.p_bid_price}</Text></View>
                      {item.timeduration>0?<CardItem footer>
                        <TouchableOpacity onPress={()=>this.open(item.pid,item.p_bid_price)} style={{backgroundColor:'#DE6F10',width:screenwidth/3.5,borderRadius:3,height:30,justifyContent:'center',alignItems:'center'}}>
                        
                        <Text style={{fontSize:13,color:'#fff'}}>BID NOW</Text>
                        </TouchableOpacity>
                      </CardItem>:<View/>}  
                    <View style={{paddingBottom:10}}>
                    <View style={{flexDirection:'row',paddingTop:3,fontSize:13}}><Text style={{color:'#008000'}}>Product Price:</Text><Text style={{paddingLeft:2,color:'#008000'}}>₹{item.p_price}</Text></View>
                    </View> 
                    {item.timeduration<0?
                    <View style={{alignItems:'center',justifyContent:'center'}}><Text style={{fontSize:20,color:'#DE6F10',paddingBottom:5}}>CLOSED</Text></View>:
                    <View/>}
                </Card>
                
                      )}
                   />

          </View>

        </Content>
        <Modal
        offset={0}
        closeOnTouchOutside={false} disableOnBackPress={true}
        open={this.state.open}
        modalDidOpen={this.modalDidOpen}
        modalDidClose={this.modalDidClose}
        style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
        <View style={{alignContent:'center',alignItems:'center'}}>
        <Spinner color='#DE6F10' />
          </View>
        </Modal>
      <Modal1 offset={0} open={this.state.open1} modalDidOpen={this.modalDidOpen1} 
                modalDidClose={this.modalDidClose1} style={{ alignItems: "center", }}>
                
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
                <Left></Left>
                <Text style={{ fontSize: 20, marginBottom: 10, textAlign: 'center' }}>BID AND WIN</Text>
                
                <Right/>
            </View>
        <View style={{ flexDirection:'column' ,alignItems: 'center', justifyContent: 'center' }}>
        <Item regular style={{borderColor:'black',height:40,width:screenwidth/1.5}}>
            <Input placeholder='Enter Bid' value={this.state.bitval} onChangeText={(bitval) => this.setState({ bitval: bitval })}/>
          </Item>
          <TouchableOpacity onPress={()=>this.bid()} style={{marginTop:15,height: 50,width:screenwidth/2.5,backgroundColor: "#DE6F10",borderRadius:5,
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
         >
            <Text style={{ color: "white", fontSize: 20 }}>Submit</Text>
          </TouchableOpacity>
          </View>
</Modal1>
        </Container>
        );
    }

}
const styles = StyleSheet.create({
    gridView: {
    //   padding:5,
      flex: 1,
      backgroundColor:'#fff',
    },
    
  })

