
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  AsyncStorage,
  TouchableOpacity,
  StatusBar, Alert,
  ImageBackground,
  BackHandler,Linking,WebView
} from 'react-native';
import {
  Container,
  Content,
  Spinner,
  Footer,
  FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input
} from 'native-base';
import {AdMobInterstitial} from 'react-native-admob';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
export default class Spinandwin extends Component {
  state={
    user_id:''
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };
  componentWillMount(){
    AdMobInterstitial.setAdUnitID('ca-app-pub-3493193453765061/9006834471');
    AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
    AsyncStorage.getItem('user')
    .then((value) => {
        if (value != null || value != undefined) {
            const values = JSON.parse(value)
            console.log('user available' + values.userid)
            console.log(values)
            this.setState({user:values,user_id:values.userid,referal:values.referal})
        }
      })
  }
  render() {
    return (
      <Container>
      <Header style={{ backgroundColor: '#047BD5' }}>
        <Left>
        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
        <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
        </TouchableOpacity>
        </Left>
        <Body><Title>Spin and Win</Title></Body>
        </Header>
        <StatusBar backgroundColor="#1565C0"/>
      <View style={styles.container} >
        <WebView
        source={{uri: 'http://payoja.co/wheel/index.php?id='+this.state.user_id}}
        style={{padding:20,width:screenwidth,height:screenHeight}}
        scalesPageToFit={true}
        scrollEnabled={false}
        renderLoading={this.renderLoadingView}
      /> 
  </View>
  </Container>
    );
  }
}
let styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
  },
  webView: {
      backgroundColor: '#fff',
      height: 350,
  }
  });
