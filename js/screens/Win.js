import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,
  } from 'react-native';
import {
    Container,Header,Left,Body,Card,
    Content,
    Spinner,
    Footer,
    FooterTab,
    Icon,Title
  } from 'native-base';
  const screenwidth=Dimensions.get('window').width;
  const screenHeight=Dimensions.get('window').height;
  import urldetails from '../config/endpoints.json';
  import Modal from "react-native-simple-modal";
  import Imagel from 'react-native-image-progress';
  import ProgressBar from 'react-native-progress/Bar';
  import {
    AdMobBanner,
    AdMobRewarded,
    AdMobInterstitial,
    PublisherBanner,
  } from 'react-native-admob';
export default class Win extends Component {
  state={
    winningdata:[],
    data:'',
    otherwinner:[]
  }
  componentWillMount() {
  // AdMobInterstitial.setAdUnitID('ca-app-pub-3493193453765061/1647628322');
  // AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
    this.setState({open:true})
    AsyncStorage.getItem('user')
        .then((value) => {
            if (value != null || value != undefined) {
                const values = JSON.parse(value)
                console.log('user available' + values.userid)
                this.setState({ useridval: values.userid})
             this.getwinner()
            }
        })
  }
   componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });
 getwinner(){
    var formData= new FormData();
    formData.append('user_id',this.state.useridval);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["winner"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success")
    {
      this.setState({winningdata:jsondata.data,data:jsondata.status,open:false,otherwinner:jsondata.otherwinner})
    }
    else{
      this.setState({open:false,data:jsondata.status,otherwinner:jsondata.otherwinner})
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.getwinner()},
      ],
      { cancelable: false }
    )
    })
 }
  render() {
    return (
        <Container>
        <Header style={{ backgroundColor: '#047BD5' }}>
          <Left>
          <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
          <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
          </TouchableOpacity>
          </Left>
          <Body><Title>WINNER</Title></Body>
          </Header>
          <StatusBar backgroundColor="#1565C0"/>
          <Content style={{backgroundColor:'#fff'}}>
          {this.state.data=="success"?
          <View style={{alignItems:'center',justifyContent:'center'}}>
           <Image source={{uri:'https://i.pinimg.com/originals/e7/bb/a8/e7bba8cfe31a357998e454d5bf84ab2a.jpg'}}
                  style={{width:screenwidth/3,height:screenHeight/6}} resizeMode="contain" />
          <Text style={{padding:10,fontSize:20}}>Congratulation You won the bidding</Text>

          {
            this.state.winningdata.map((item,i)=>
            {
              return(

          <Card style={{width:screenwidth/1.5,alignItems:'center',justifyContent:'center',borderColor:'#000',borderWidth:0.5}}>
          <Text style={{padding:5,fontSize:15,color:'#000'}}>{item.products}</Text>
                   <Imagel source={{ uri: urldetails.image + item.image}} indicator={ProgressBar.Pie}
                                      indicatorProps={{ size: 80, borderWidth: 0, color: 'rgba(150, 150, 150, 1)', unfilledColor: 'rgba(200, 200, 200, 0.2)' }}
                                      style={{width:screenwidth/3,height:screenHeight/6}} resizeMode="contain"/>
                   <Text style={{padding:5,fontSize:20}}>₹{item.price}</Text>

          </Card>
              )
            })
          }
          { this.state.otherwinner.map((data,j)=>
            {
              return(
                <Card style={{width:screenwidth/1.5,alignItems:'center',justifyContent:'center',}} key={j}>
          <Text style={{padding:5,fontSize:15,color:'#000'}}>{data.productname}</Text>
                   <Imagel source={{ uri: urldetails.image + item.data}} indicator={ProgressBar.Pie}
                                      indicatorProps={{ size: 80, borderWidth: 0, color: 'rgba(150, 150, 150, 1)', unfilledColor: 'rgba(200, 200, 200, 0.2)' }}
                                      style={{width:screenwidth/3,height:screenHeight/6}} resizeMode="contain"/>
                   <Text style={{padding:5,fontSize:20,color:'#008000'}}>{data.winnename}</Text>
          </Card> 
              )}
          )
          }
           <AdMobBanner
              adSize="banner"
              adUnitID="ca-app-pub-3493193453765061/7202798216"
            />  
          </View>:  <View style={{alignItems:'center',justifyContent:'center'}}>
          {this.state.otherwinner.map((item,i)=>{
            return(
              <Card style={{width:screenwidth/1.5,alignItems:'center',justifyContent:'center',}} key={i}>
          <Text style={{padding:5,fontSize:15,color:'#000'}}>{item.productname}</Text>
                   <Imagel source={{ uri: urldetails.image + item.image}} indicator={ProgressBar.Pie}
                                      indicatorProps={{ size: 80, borderWidth: 0, color: 'rgba(150, 150, 150, 1)', unfilledColor: 'rgba(200, 200, 200, 0.2)' }}
                                      style={{width:screenwidth/3,height:screenHeight/6}} resizeMode="contain"/>
                   <Text style={{padding:5,fontSize:20,color:'#008000'}}>{item.winnename.toUpperCase()}</Text>
          </Card> 
            )}
            )
          }
             <AdMobBanner
              adSize="banner"
              adUnitID="ca-app-pub-3493193453765061/7202798216"
            /> 
          </View>} 
         </Content>
         <Modal
            offset={0}
            closeOnTouchOutside={false} disableOnBackPress={true}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#DE6F10' />
              </View>
            </Modal>
       
       </Container>

    );
  }
}
