import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
import {
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab,
  } from 'native-base';
  import {
    AdMobBanner,
    AdMobRewarded,
    AdMobInterstitial,
    PublisherBanner,
  } from 'react-native-admob';
export default class Splash extends Component {
  componentWillMount() {
    AdMobInterstitial.setAdUnitID('ca-app-pub-3493193453765061/3590856268');
     AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
    AsyncStorage.getItem('user')
        .then((value) => {
            if (value != null || value != undefined) {
                const values = JSON.parse(value)
                console.log('user available' + values.userid)
              //   this.setState({ useridval: values.userid, name:values.username,email:values.email,contact:values.contact,password:values.password })
                //this.userProfile()
                
                setTimeout(() => this.props.navigation.navigate('Home'), 3000);
            }
            else{
              setTimeout(() => this.props.navigation.navigate('Login'), 5000);
            }
        })
        // AdMobInterstitial.setAdUnitID('ca-app-pub-3493193453765061/3590856268');

        // this.showInterstitial()
  }
  // componentDidMount(){
  //   AdMobInterstitial.addEventListener('adLoaded',
  //     () => console.log('AdMobInterstitial adLoaded')
  //   );
  //   AdMobInterstitial.addEventListener('adFailedToLoad',
  //     (error) => console.warn(error)
  //   );
  //   AdMobInterstitial.addEventListener('adOpened',
  //     () => console.log('AdMobInterstitial => adOpened')
  //   );
  //   AdMobInterstitial.addEventListener('adClosed',
  //     () => {
  //       console.log('AdMobInterstitial => adClosed');
  //       AdMobInterstitial.requestAd().catch(error => console.warn(error));
  //     }
  //   );
  //   AdMobInterstitial.addEventListener('adLeftApplication',
  //     () => console.log('AdMobInterstitial => adLeftApplication')
  //   );

  //   AdMobInterstitial.requestAd().catch(error => console.warn(error));
  // }
  
  // showInterstitial() {
  //   AdMobInterstitial.showAd().catch(error => console.warn(error));
  // }

  render() {
    return (
        <Container>
        <Content style={{backgroundColor:'#047BD5'}}>
      <View style={{ flex: 1, alignItems: 'center' }}>
          <StatusBar backgroundColor="#1565C0" barStyle="light-content"/>
          <View style={{alignItems: 'center', justifyContent: 'center',marginTop:100}}>
          <Image
                  style={{ width: 180, height: 180, resizeMode: 'contain', alignItems: 'center', justifyContent: 'center' }}
                  source={require('../assets/logo.png')}
              />
              <Spinner color='#fff' style={{paddingTop:30}}/>
              {/* <AdMobBanner
              adSize="banner"
              adUnitID="ca-app-pub-3493193453765061/5201576869"
              
            />  */}
          </View>
        
            
      </View>
    
      </Content>
      </Container>

    );
  }
}
