
import React, {Component} from 'react';
import {Platform,TouchableOpacity,StyleSheet, Text, View,Dimensions,StatusBar,Alert,BackHandler} from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label,Icon,Right,Spinner } from 'native-base';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import Modal from "react-native-simple-modal";
import urldetails from '../config/endpoints.json';
export default class Login extends Component {
  state = {
    isFocused: false,
    email:''
  };
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Login");
    return true;
  };
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });
  forgotpassword(){
    this.setState({open:true})
    var formData= new FormData();
    formData.append('u_email',this.state.email);
  console.log(formData)
  fetch(urldetails.base_url+urldetails.url["forgotpassword"],{
    method:"POST",
    body:formData
  })
  .then((response)=>response.json())
.then((jsondata)=>{
  console.log(jsondata)
  if(jsondata.status=="success"){
    this.setState({open:false})
    Alert.alert(
      'Alert',
      "Mail sent.Please check your Email",
      [
        {text: 'Ok', onPress: () =>this.props.navigation.navigate("Login")},
      ],
      { cancelable: false })
  }
  else{
    this.setState({open:false})
    Alert.alert(
      'Alert',
      "Please enter Registered Email")
  }
}) .catch((error) => {
  console.log(error)
  Alert.alert(
    'Alert',
    "Please check your Internet Connection.",
    [
      {text: 'Retry', onPress: () =>this.forgotpassword()},
    ],
    { cancelable: false }
  )
  });
  
  }
  render() {
    return (
      <Container>
      <StatusBar backgroundColor="#1565C0"/>
     <Content style={{backgroundColor:'#047BD5'}}>
    <View style={{alignItems:'center',marginTop:170,justifyContent:'center'}}>
       <Text style={{padding:20,fontSize:25,color:'#fff'}}>FORGOT PASSWORD</Text>
       <Item regular style={{width:screenwidth-50,height:50,marginTop:20,borderRadius:5,borderColor: '#fff',borderWidth:0.5}}>
          <Icon name='ios-mail' style={{color:'#fff',marginLeft:3}}/>
          <Input placeholder='Email' placeholderTextColor='#fff'style={{color:'#fff',paddingLeft:5}} onChangeText={(email)=>this.setState({email:email.toLowerCase()})} />
      </Item>
        <TouchableOpacity style={{width:screenwidth-40,height:screenHeight/15,marginTop:30,backgroundColor:'#fff',borderColor:'#fff',borderWidth:1,borderRadius:5,alignContent:'center',alignItems:'center',justifyContent:'center'}}
        onPress={()=>this.forgotpassword()}>
        <Text style={{fontSize:20,color:'#DE6F10'}}>Submit</Text>
        </TouchableOpacity>
    </View>
    </Content>
    <Modal
        offset={0}
        closeOnTouchOutside={false} disableOnBackPress={true}
        open={this.state.open}
        modalDidOpen={this.modalDidOpen}
        modalDidClose={this.modalDidClose}
        style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
        <View style={{alignContent:'center',alignItems:'center'}}>
        <Spinner color='#DE6F10' />
        </View>
        </Modal>
  </Container>
    );
  }
}

