
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
import {
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab,Icon,Header,Left,Body,Title,List,ListItem,Right,Item,Input
  } from 'native-base';
  const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
import Modal from "react-native-simple-modal";
import Modal1 from "react-native-simple-modal";
import Modal2 from "react-native-simple-modal";
import Modal3 from "react-native-simple-modal";
import Modal4 from "react-native-simple-modal";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {
  AdMobBanner,
  AdMobRewarded,
  AdMobInterstitial,
  PublisherBanner,
} from 'react-native-admob';
export default class Account extends Component {
  state={
    user_id:'',
    userdata:[],
    accountdata:[],
    name:'',
    accountspin:[],
    referalid:'',
    open2:false,open4:false,
    accname:'',
    paytmno:'',
    ifsc:'',
    accno:''
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  componentWillMount(){
    this.setState({open:true})
    AsyncStorage.getItem('user')
    .then((value) => {
        if (value != null || value != undefined) {
            const values = JSON.parse(value)
            console.log('user available' + values.userid)
            console.log(values)
            this.setState({user_id:values.userid,username:values.username,email:values.email,contact:values.contact})
            console.log(this.state.user_id)
            this.viewuser(this.state.user_id)
            // this.viewaccount(this.state.user_id)
            this.viewspin(this.state.user_id)

        }
        
      })
  }
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });
  //modal for edit profile------------
  modalDidOpen1 = () => console.log("Modal did open");
  modalDidClose1 = () => {
    this.setState({ open1: false });
  };
  openModal1 = () => this.setState({ open1: true });
  closeModal1 = () => this.setState({ open1: false });
  //-------------------------------
   //modal for referal------------
   modalDidOpen2 = () => console.log("Modal did open");
   modalDidClose2 = () => {
     this.setState({ open2: false });
   };
   openModal2 = () => this.setState({ open2: true });
   closeModal2 = () => this.setState({ open2: false });
   //-------------------------------
     //modal for select mode------------
     modalDidOpen3 = () => console.log("Modal did open");
     modalDidClose3 = () => {
       this.setState({ open3: false });
     };
     openModal3 = () => this.setState({ open3: true });
     closeModal3 = () => this.setState({ open3: false });
     //-------------------------------
       //modal for paytm------------
       modalDidOpen4 = () => console.log("Modal did open");
       modalDidClose4 = () => {
         this.setState({ open4: false });
       };
       openModal4 = () => this.setState({ open4: true });
       closeModal4 = () => this.setState({ open4: false });
       //-------------------------------
  viewuser(user_id){
    var formData= new FormData();
    formData.append('user_id',user_id);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["viewuser"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      this.setState({userdata:jsondata.data,open:false,accountdata:jsondata.account,referal:jsondata.referal})
      console.log(jsondata.referal)
    }
    else{
      this.setState({open:false}) 
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.viewuser(this.state.user_id)},
      ],
      { cancelable: false }
    )
    });
  }
  viewspin(user_id){
    var formData= new FormData();
    formData.append('user_id',user_id);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["creditspins"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      this.setState({accountspin:jsondata.accounts})
      console.log(this.state.accountspin)
    }
    else{
      this.setState({accountspin:jsondata.accounts})
      this.setState({open:false}) 
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.viewspin(this.state.user_id)},
      ],
      { cancelable: false }
    )
    });
  }
  deleteaccount(){
    Alert.alert(
      'Alert',
      "Are You sure to delete your account",
      [
        {text: 'Yes', onPress: () =>this.deleteaccountyes()},
        {text: 'No', onPress: () =>this.props.navigation.navigate("Account")},
      ],
      { cancelable: false }
    )
  }
  deleteaccountyes(){
    this.setState({open:true})
    var formData= new FormData();
    formData.append('user_id',this.state.user_id);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["deleteuser"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      this.setState({open:false})
      Alert.alert(
        'Alert',
        "Account Deleted")
        AsyncStorage.setItem('user',"");
        this.props.navigation.navigate("Login")
    }
    else{
      Alert.alert(
        'Alert',
        "Try Again")
    }
    })
  }
  editprofile(){
    this.setState({open:true})
    var formData= new FormData();
    formData.append('user_id',this.state.user_id);
    formData.append('u_name',this.state.name);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["editprofile"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      this.viewuser(this.state.user_id)
      this.setState({open:false,open1:false})
      Alert.alert(
        'Alert',
        jsondata.message)
        var userdetails={
          userid:jsondata.data['user_id'],
          username:jsondata.data['u_name'],
          email:jsondata.data['u_email'],
          contact:jsondata.data['u_phoneno'],
          referalid:jsondata.data['u_referalid'],
          referal:jsondata.referal
        }
        console.log(userdetails)
        AsyncStorage.setItem('user',JSON.stringify(userdetails));
        this.props.navigation.navigate("Redirect")
    }
    else{
      Alert.alert(
        'Alert',
        "Try Again")
    }
    })

  }
  payoutbank(){
  console.log(this.state.accountdata.acc_credits/100)
  this.setState({open1:false,open:true})
    var formData= new FormData();
    formData.append('user_id',this.state.user_id);
    formData.append('user_bankacc_no',this.state.accno);
    formData.append('user_bankifsc_code',this.state.ifsc);
    formData.append('paytm_phoneno',0);
    formData.append('user_name','NA');
    formData.append('amount',this.state.accountdata.acc_credits);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["payout"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success")
    {
      this.setState({open:false,open1:false,open4:false,accno:'',ifsc:'',accname:'',paytmno:''})
      Alert.alert('',jsondata.message)
    }
    else{
      this.setState({open:false,open1:false,open4:false})
      Alert.alert('',jsondata.message)
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.payoutbank()},
      ],
      { cancelable: false }
    )
    });
  
  }
  payoutpaytm(){
    console.log(this.state.accountdata.acc_credits)
    this.setState({open1:false,open:true})
      var formData= new FormData();
      formData.append('user_id',this.state.user_id);
      formData.append('user_bankacc_no',0);
      formData.append('user_bankifsc_code','NA');
      formData.append('paytm_phoneno',this.state.paytmno);
      formData.append('user_name',this.state.accname);
      formData.append('amount',this.state.accountdata.acc_credits);
      console.log(formData)
      fetch(urldetails.base_url+urldetails.url["payout"],{
        method:"POST",
        body:formData
      })
      .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)
      if(jsondata.status=="success")
      {
        this.setState({open:false,open1:false,open4:false,accno:'',ifsc:'',accname:'',paytmno:''})
        Alert.alert('',jsondata.message)
      }
      else{
        this.setState({open:false,open1:false,open4:false})
        Alert.alert('',jsondata.message)
      }
    }).catch((error) => {
      console.log(error)
      Alert.alert(
        'Alert',
        "Please check your Internet Connection.",
        [
          {text: 'Retry', onPress: () =>this.payoutpaytm()},
        ],
        { cancelable: false }
      )
      });
    
    }
  referal(){
    var formData= new FormData();
    formData.append('user_id',this.state.user_id);
    formData.append('u_referalid',this.state.referalid);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["referals"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      Alert.alert(
        'Alert',
        "Congtatulations.You won Rs.5",
      )
      this.setState({open2:false})
      this.componentWillMount()
    }
    else{
      this.setState({open:false})
      Alert.alert(
        'Alert',
        "Try Again.",
      )
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.referal()},
      ],
      { cancelable: false }
    )
    });
  }
  render() {
    return (
        <Container>
        <Content style={{backgroundColor:'#f5f5f5'}}>
        <Header style={{ backgroundColor: '#047BD5' }}>
          <Left>
          <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
          <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
          </TouchableOpacity>
          </Left>
          <Body><Title>ACCOUNT</Title></Body>
          </Header>
          <StatusBar backgroundColor="#1565C0"/>
          <View style={{alignContent:'center',alignItems:'center'}}>
        <View style={{paddingTop:10,borderBottomColor:'#000',borderwidth:2}}>
          <View  style={{flexDirection:'row',backgroundColor:'#EBEBEB',width:screenwidth-20,height:screenHeight/17,borderRadius:10,alignContent:'center',alignItems:'center'}}>
          <Icon style={{  fontSize: 20,paddingLeft:10,color:'#DE6F10' }} name='md-person' />
          <Text style={{paddingLeft:'10%',fontSize:15,color:'#000'}}>Name</Text>
              <Right style={{flexDirection:'row',paddingLeft:'50%',}}>
          <Text style={{fontSize:15,color:'#047BD5'}}>{this.state.userdata.u_name}</Text>
          {/* <FontAwesome style={{ color: '#000', paddingLeft: 5,fontSize: 16 }} name="pencil-square-o" /> */}
          </Right>
            </View>
            <View style={{flexDirection:'row',marginTop:10,backgroundColor:'#fff',width:screenwidth-20,height:screenHeight/17,borderRadius:10,alignContent:'center',alignItems:'center'}}>
          <Icon style={{  fontSize: 20,paddingLeft:10,color:'#DE6F10' }} name='md-mail' />
          <Text style={{paddingLeft:'10%',fontSize:15,color:'#000'}}>Email</Text>
          <Right>
          <Text style={{paddingRight:10,fontSize:15,color:'#047BD5'}}>{this.state.userdata.u_email}</Text>
          </Right>
            </View>
            <View style={{flexDirection:'row',marginTop:10,backgroundColor:'#fff',width:screenwidth-20,height:screenHeight/17,borderRadius:10,alignContent:'center',alignItems:'center'}}>
          <Icon style={{  fontSize: 20,paddingLeft:10,color:'#DE6F10' }} name='md-call' />
          <Text style={{paddingLeft:'10%',fontSize:15,color:'#000'}}>Phone Number</Text>
          <Right>
          <Text style={{paddingRight:10,fontSize:15,color:'#047BD5'}}>{this.state.userdata.u_phoneno}</Text>
          </Right>
            </View>
            </View>
            <View style={{backgroundColor:'#f0f0f0',width:screenwidth-10,marginTop:5,}}>
            <List>
                <ListItem style={{flexDirection:'row'}}>
                <FontAwesome5 style={{ color: '#DE6F10', paddingLeft: 5,fontSize: 20 }} name="rupee-sign" />
                <Text style={{paddingLeft:'10%',fontSize:15,color:'#000'}}>Account Credits:</Text>
                <Text style={{paddingLeft:10,fontSize:15,color:'#000'}}>₹{this.state.accountdata.acc_credits/100}</Text>
                 </ListItem>   
                 <ListItem style={{flexDirection:'row'}}>
                 <FontAwesome style={{ color: '#DE6F10', paddingLeft: 5,fontSize: 20 }} name="gift" />
                 <Text style={{paddingLeft:'10%',fontSize:15,color:'#000'}}>Paid Bids:</Text>
                 <Text style={{paddingLeft:10,fontSize:15,color:'#000'}}>{this.state.accountdata.acc_paidbids}</Text>
                 </ListItem> 
                 <ListItem style={{flexDirection:'row'}}>
                 <FontAwesome style={{ color: '#DE6F10', paddingLeft: 5,fontSize: 20 }} name="gift" />
                 <Text style={{paddingLeft:'10%',fontSize:15,color:'#000'}}>Free Bids:</Text>
            <Text style={{paddingLeft:10,fontSize:15,color:'#000'}}>{this.state.accountdata.acc_freebids}</Text>
                 </ListItem> 
                 <ListItem style={{flexDirection:'row'}}>
                 <FontAwesome style={{ color: '#DE6F10', paddingLeft: 5,fontSize: 20 }} name="gift" />
                 <Text style={{paddingLeft:'10%',fontSize:15,color:'#000'}}>Number of Spin:</Text>
            <Text style={{paddingLeft:10,fontSize:15,color:'#000'}}>{this.state.accountdata.acc_spins}</Text>
                 </ListItem> 
            </List>    
           
            </View>
            <View style={{backgroundColor:'#fff',width:screenwidth-10,marginTop:5,}}>
            <List>
              {this.state.accountdata.acc_credits/100>=50?
            <ListItem style={{flexDirection:'row'}} onPress={()=>this.setState({open3:true})}>
            <Icon style={{ color: '#DE6F10', paddingLeft: 5,fontSize: 20 }} name="ios-radio-button-on" />
                <Text style={{paddingLeft:'10%',fontSize:17,color:'#000'}}>PayOut</Text>
                 </ListItem>:<View/>}  
                <ListItem style={{flexDirection:'row'}} onPress={() => this.props.navigation.navigate(('Changepassword'),{useridval:this.state.useridval})}>
                <Icon style={{ color: '#DE6F10', paddingLeft: 5,fontSize: 20 }} name="ios-radio-button-on" />
                <Text style={{paddingLeft:'10%',fontSize:17,color:'#000'}}>Change Password</Text>
                 </ListItem>   
                 <ListItem style={{flexDirection:'row'}} onPress={() =>this.deleteaccount()}>
                 <Icon style={{ color: '#DE6F10', paddingLeft:5,fontSize: 20 }} name="ios-radio-button-on" />
                <Text style={{paddingLeft:'10%',fontSize:17,color:'#000'}}>Delete Account</Text>
                 </ListItem>
                 {this.state.referal=="false"?
                 <ListItem style={{flexDirection:'row'}} onPress={() =>this.setState({open2:true})}>
                 <Icon style={{ color: '#DE6F10', paddingLeft:5,fontSize: 20 }} name="ios-radio-button-on" />
                <Text style={{paddingLeft:'10%',fontSize:17,color:'#000'}}>Have a Referalcode?</Text>
              </ListItem>:<View/>}     
                 
            </List>    
           
            </View>
           <AdMobBanner
              adSize="banner"
              adUnitID="ca-app-pub-3493193453765061/4848506731"
            /> 
         </View>
      </Content>
      <Modal
        offset={0}
        closeOnTouchOutside={false} disableOnBackPress={true}
        open={this.state.open}
        modalDidOpen={this.modalDidOpen}
        modalDidClose={this.modalDidClose}
        style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
        <View style={{alignContent:'center',alignItems:'center'}}>
        <Spinner color='#DE6F10' />
          </View>
        </Modal>
        <Modal1 offset={0} open={this.state.open1} modalDidOpen={this.modalDidOpen1} 
                modalDidClose={this.modalDidClose1} style={{ alignItems: "center", }}>
                
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
                <Left></Left>
                <Text style={{ fontSize: 20, marginBottom: 10, textAlign: 'center' }}>PayOut</Text>
                <Right>
                    <TouchableOpacity style={{ backgroundColor: '#000', borderRadius: 25, width: 25, height: 25 }} onPress={() => this.setState({ open1: false })} >
                    <Text style={{ color: '#fff', fontWeight: 'bold', padding: 2, textAlign: 'center' }}>X</Text>
                    </TouchableOpacity>
                </Right>
            </View>
        <View style={{ flexDirection:'column' ,alignContent: 'center', justifyContent: 'center',height:screenHeight/3 }}>
        {/* <Item regular style={{borderColor:'black'}}>
            <Input placeholder={this.state.name} value={this.state.usrname} onChangeText={(usrname) => this.setState({ usrname: usrname })}/>
          </Item> */}
          <Item regular style={{borderColor:'black',borderRadius:5}}>
            <Input placeholder='Enter Account Number' value={this.state.accno} onChangeText={(accno) => this.setState({ accno: accno})}/>
          </Item>
          <Item regular style={{borderColor:'black',marginTop:4,borderRadius:5}}>
            <Input placeholder='Enter IFSC code' value={this.state.ifsc} onChangeText={(ifsc) => this.setState({ ifsc: ifsc })}/>
          </Item>
          <TouchableOpacity  style={{marginTop:15,height: 50,backgroundColor: "#DE6F10",borderRadius:5,
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
          onPress={() => this.payoutbank()}>
            <Text style={{ color: "white", fontSize: 20 }}>Submit</Text>
          </TouchableOpacity>
          </View>
       </Modal1>
       <Modal2 offset={0} open={this.state.open2} modalDidOpen={this.modalDidOpen2} modalDidClose={this.modalDidClose2} style={{ backgroundColor: "#fff" }}>
      <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', }}>
                <Left></Left>
                <Right>
                    <TouchableOpacity style={{ backgroundColor: 'black', borderRadius: 25, width: 25, height: 25 }} onPress={() => this.setState({open2:false})} >
                    <Text style={{ color: '#fff', fontWeight: 'bold', padding: 3, textAlign: 'center' }}>X</Text>
                    </TouchableOpacity>
                </Right>
            </View>
            <View style={{ flexDirection:'column' ,alignItems: 'center',backgroundColor:'#fff', justifyContent: 'center',marginTop:5 }}>
        <Image source={{uri:'http://www.valldaro.com/wp-content/uploads/2013/03/gift.png'}} 
        style={{width:screenwidth/3,height:screenHeight/6,marginTop:5}}/>
         <Text style={{ fontSize: 16, marginBottom: 10,color:"#047BD5", textAlign: 'center' }}>Enter your friend's referal code to receive ₹5</Text>
        <Item regular style={{borderColor:'#000'}} style={{width:screenwidth/1.5,height: 50,borderColor:'#000',borderWidth:2}}>
            <Input  onChangeText={(referalid) => this.setState({ referalid: referalid })}/>
          </Item>
          <TouchableOpacity  style={{marginTop:15,width:screenwidth/1.2,height: 50,backgroundColor: "#DE6F10",borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
          onPress={() => this.referal()}>
            <Text style={{ color: "white", fontSize: 20 }}>Submit</Text>
          </TouchableOpacity> 
    </View>
          </Modal2>
          <Modal3 offset={0} open={this.state.open3} modalDidOpen={this.modalDidOpen3} modalDidClose={this.modalDidClose3} style={{ backgroundColor: "#fff" }}>
      <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', }}>
                <Left></Left>
                <Right>
                    <TouchableOpacity style={{ backgroundColor: 'black', borderRadius: 25, width: 25, height: 25 }} onPress={() => this.setState({open3:false})} >
                    <Text style={{ color: '#fff', fontWeight: 'bold', padding: 3, textAlign: 'center' }}>X</Text>
                    </TouchableOpacity>
                </Right>
            </View>
            <View style={{ flexDirection:'column' ,alignItems: 'center', justifyContent: 'center',marginTop:5 }}>
         <Text style={{ fontSize: 20, marginBottom: 10,color:"#047BD5", textAlign: 'center' }}>Select Your Pament Mode</Text>
         <View style={{flexDirection:'row',marginTop:15}}>
          <TouchableOpacity onPress={()=>this.setState({open1:true,open3:false})} style={{width:screenwidth/2-40,height: 50,backgroundColor: "#DE6F10",borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
          >
            <Text style={{ color: "white", fontSize: 15 }}>Bank Account</Text>
          </TouchableOpacity> 
          <TouchableOpacity onPress={()=>this.setState({open4:true,open3:false})} style={{marginLeft:4,width:screenwidth/2-40,height: 50,backgroundColor: "#DE6F10",borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
          >
            <Text style={{ color: "white", fontSize: 15 }}>Paytm</Text>
          </TouchableOpacity> 
          </View>
    </View>
          </Modal3>
          <Modal4 offset={0} open={this.state.open4} modalDidOpen={this.modalDidOpen4} 
                modalDidClose={this.modalDidClose4} style={{ alignItems: "center", }}>
                
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
                <Left></Left>
                <Text style={{ fontSize: 20, marginBottom: 10, textAlign: 'center' }}>PayOut</Text>
                <Right>
                    <TouchableOpacity style={{ backgroundColor: '#000', borderRadius: 25, width: 25, height: 25 }} onPress={() => this.setState({ open4: false })} >
                    <Text style={{ color: '#fff', fontWeight: 'bold', padding: 2, textAlign: 'center' }}>X</Text>
                    </TouchableOpacity>
                </Right>
            </View>
        <View style={{ flexDirection:'column' ,alignContent: 'center', justifyContent: 'center',height:screenHeight/3 }}>
        {/* <Item regular style={{borderColor:'black'}}>
            <Input placeholder={this.state.name} value={this.state.usrname} onChangeText={(usrname) => this.setState({ usrname: usrname })}/>
          </Item> */}
          <Item regular style={{borderColor:'black',borderRadius:5}}>
            <Input placeholder='Enter Your Name' value={this.state.accname} onChangeText={(accname) => this.setState({ accname: accname})}/>
          </Item>
          <Item regular style={{borderColor:'black',marginTop:4,borderRadius:5}}>
            <Input placeholder='Enter Paytm Number' value={this.state.paytmno} onChangeText={(paytmno) => this.setState({ paytmno: paytmno })}/>
          </Item>
          <TouchableOpacity  style={{marginTop:15,height: 50,backgroundColor: "#DE6F10",borderRadius:5,
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
          onPress={() => this.payoutpaytm()}>
            <Text style={{ color: "white", fontSize: 20 }}>Submit</Text>
          </TouchableOpacity>
          </View>
       </Modal4>
        </Container>

    );
  }
}



