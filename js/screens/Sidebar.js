import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  StatusBar,
  Platform,
  TouchableWithoutFeedback,
  Dimensions,
  AsyncStorage,
} from 'react-native';
import {
  Container,
  Content,
  Body,
  Header,
  Label,
  ListItem,
  Thumbnail,Icon
} from 'native-base';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
var navitems =[
  {name:'Home',nav:'Home',image:<Icon name='ios-home'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#DE6F10'}}/>},
  {name:'Account',nav:'Account',image:<Icon name='ios-person'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#DE6F10'}}/>},
  {name:'Spin and Win',nav:'Spinandwin',image:<Icon name='ios-flower'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#DE6F10'}}/>},
  {name:'Bid Shop',nav:'Bidshop',image:<Icon name='ios-cart'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#DE6F10'}}/>},
  // {name:'Transaction',nav:'Home',image:<Icon name='ios-paper'
  // style={{fontSize:30,marginTop:8,marginBottom:8,color:'#DE6F10'}}/>},
  // {name:'Refer and Win',nav:'Home',image:<Icon name='ios-arrow-dropright-circle-outline'
  // style={{fontSize:30,marginTop:8,marginBottom:8,color:'#047BD5'}}/>},
  {name:'Win History',nav:'Win',image:<Icon name='ios-trophy'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#DE6F10'}}/>},
  {name:'Logout',nav:'Logout',image:<Icon name='ios-power'
  style={{fontSize:30,marginTop:8,marginBottom:8,color:'#DE6F10'}}/>},
]
export default class Sidebar extends Component {
  state={
    username:''
  }
  componentWillMount(){
    AsyncStorage.getItem('user')
    .then((value) => {
        if (value != null || value != undefined) {
            const values = JSON.parse(value)
            console.log('user available' + values.userid)
            console.log(values)
            this.setState({user_id:values.userid,username:values.username,email:values.email.toLowerCase()})
            console.log(this.state.username)
           
        }
        
      })
  }
  render() {
    return (
        <Container>
           <View style={{height:160,alignItems:"center",
                      justifyContent: 'center',backgroundColor:'#047BD5',}}>
                      <Image source={{uri:'http://naapbooks.com/wp-content/uploads/2018/10/user1.png'}} style={{width:80, height:80}} />
            <Text style={{color:'#fff', backgroundColor:'transparent', padding:4, textAlign:'center',fontSize:18}}>{this.state.username}</Text>
            <Text style={{color:'#fff', backgroundColor:'transparent', padding:4, textAlign:'center',fontSize:15}}>{this.state.email}</Text>
          </View>
      <View style={{borderWidth:0, flex:1, backgroundColor:'white'}}>
        <Content style={{backgroundColor:'#fff'}}>
        <View>
          {navitems.map((l,i,)=>{
            return (<ListItem key={i} style={{height:50}} onPress={()=>{
                  this.props.navigation.toggleDrawer(),
                  this.props.navigation.navigate(l.nav)}}>
            <View style={{flexDirection:'row',backgroundColor:'#fff0',marginRight:50,
                              }}>
                  {l.image}
            <Text style={{fontSize:15,marginLeft:16,marginTop:16,color:'#000'}}>{l.name}</Text>
            </View></ListItem>)})
              }
        </View>
        </Content>
      </View>
      </Container>
    );
  }
}
