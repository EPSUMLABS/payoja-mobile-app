
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
import {
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input,Card, CardItem
  } from 'native-base';
  import {AdMobInterstitial} from 'react-native-admob';
  const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
// import Share, {ShareSheet, Button} from 'react-native-share';
import urldetails from '../config/endpoints.json'
import Modal from "react-native-simple-modal";
import RazorpayCheckout from 'react-native-razorpay';
export default class Bidshop extends Component {
  state={
   user_id:'',
   packagedetails:[]
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  componentWillMount(){
    // AdMobInterstitial.setAdUnitID('ca-app-pub-3493193453765061/6478967386');
    // AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
    this.setState({open:true})
    AsyncStorage.getItem('user')
    .then((value) => {
        if (value != null || value != undefined) {
            const values = JSON.parse(value)
            console.log('user available' + values.userid)
            console.log(values)
            this.setState({user_id:values.userid,username:values.username,
              email:values.email,
              contact:values.contact})
            console.log(this.state.user_id)
        }
      })
    fetch(urldetails.base_url+urldetails.url["viewbidpack"],{
        method:"GET",
      })
      .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)
      if(jsondata.status=="success"){
          this.setState({packagedetails:jsondata.data,open:false})
          console.log(this.state.packagedetails)
      }
      else{
        Alert.alert(
            'Alert',
            "ERROR",
            [
              {text: 'Ok'},
            ],
          )  
      }
    }).catch((error) => {
        console.log(error)
        Alert.alert(
          'Alert',
          "Please check your Internet Connection.",
         
        )
        });
  }
  buyusecredit(bidpack_id){
    var formData= new FormData();
    formData.append('user_id',this.state.user_id);
    formData.append('bidpack_id',bidpack_id);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["buybidpack"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      Alert.alert(
        'Alert',
        "You bought this package using credits",
      )
    }
    else{
      Alert.alert(
        'Alert',
        "You dont have enough credits to buy the package",
      )
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
    )
  });
  }
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });
  buy(bidpack_id,bidpack_price){
  var options = {
    description: 'Credits towards consultation',
    image: 'https://i.imgur.com/3g7nmJC.png',
    currency: 'INR',
    key: 'rzp_live_w7g9DI2oOim7Te',
    amount:bidpack_price+"00",
    external: {
      wallets: ['paytm']
    },
    name:this.state.name,
    prefill: {
      email:this.state.email,
      contact:this.state.contact,
      name:this.state.name
    },
    theme: {color: '#F37254'}
  }
  RazorpayCheckout.open(options).then((data) => {
    //handle success
    alert(`Success: ${data.razorpay_payment_id},`);
    this.setState({open:true})
    var formData= new FormData();
    formData.append('user_id',this.state.user_id);
    formData.append('bidpack_id',bidpack_id);
    console.log(formData)
    fetch(urldetails.base_url + urldetails.url["buybidpackage"],{
      method:"POST",
      body:formData
    }) .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)
      if(jsondata.status=="success"){
        this.setState({open:false})
        console.log(jsondata.data[0].boat_number)
        this.props.navigation.navigate("Confirm",{boatnumber:jsondata.data[0].boat_number})
      }
      else{
        Alert.alert(
          'Alert',
          jsondata.msg,
          [
            {text: 'OK', onPress: () => this.props.navigation.navigate(("Book"),
            {packagedetails:this.state.packagedetails})},
          ],
          { cancelable: false }
        )
        this.setState({open:false})
      }
    })
    .catch((error) => {
      console.log(error)
      Alert.alert(
        'Alert',
        "Please check your Internet Connection .",
        [
          {text: 'Retry', onPress: () => this.booking()},
        ],
        { cancelable: false }
      )
    });
  
    
  }).catch((error) => {
    // handle failure
    alert(`Error: ${error.code} | ${error.description}`);
  });
  RazorpayCheckout.onExternalWalletSelection(data => {
    alert(`External Wallet Selected: ${data.external_wallet} `);
  }); 
 }

  render() {
 
    return (
        <Container>
      
        <Header style={{ backgroundColor: '#047BD5' }}>
          <Left>
          <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
          <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
          </TouchableOpacity>
          </Left>
          <Body><Title>Bid Shop</Title></Body>
          </Header>
          <StatusBar backgroundColor="#1565C0"/>
          <Content style={{backgroundColor:'#fff'}}>
          <View style={{alignItems:'center',justifyContent:'center'}}>
          {
                  this.state.packagedetails.map((data, i) => {
                    return(
                      <Card style={{width:screenwidth-30,height:screenHeight/4.7,backgroundColor:'#fff'}} key={i}>
                     <View style={{flexDirection:'row',paddingTop:10}}>
                      <Left style={{paddingLeft:10}}>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Package Name : </Text>
<Text>{data.bpk_name}</Text>
</View>
<Text/>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Number of Bids : </Text>
<Text>{data.bpk_bids}</Text>
</View>
<Text/>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Price : </Text>
<Text>{data.bpk_price}</Text>
</View>
<Text/>
    </Left>
    <View style={{flexDirection:'row',paddingRight:5}}>
    <Image source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTav9u-tFIItAMD5p4rTnuUoK2KSeq8yB0_VLABNeuifnxdZK3QMQ'}}
                              style={{width:screenwidth/4,height:screenHeight/10}} />
            </View>
            </View>
            <CardItem style={{paflexDirection:'row',alignItems:'center',justifyContent:'center',padding:10}}>
    <TouchableOpacity style={{width:(screenwidth-60)/2,height:screenHeight/19,borderRadius:3,backgroundColor:'#DE6F10',justifyContent:'center',alignItems:'center'}}
    onPress={()=>this.buy(data.bidpack_id,data.bpk_price)}>
            <Text style={{color:'#fff',fontSize:15}}>Buy</Text>
            </TouchableOpacity>
            <View style={{paddingLeft:3}}>
            <TouchableOpacity onPress={()=>this.buyusecredit(data.bidpack_id)} style={{width:(screenwidth-60)/2,height:screenHeight/19,borderRadius:3,backgroundColor:'#DE6F10',justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#fff',paddingLeft:7,fontSize:15}}>Buy Using Credits</Text>
            </TouchableOpacity>
            </View>
            </CardItem>
            </Card> 
                    )}
                )}
          </View>   
          </Content>
          <Modal
            offset={0}
            closeOnTouchOutside={false} disableOnBackPress={true}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='#DE6F10' />
              </View>
            </Modal>     

      </Container>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    marginTop: 20,
    marginBottom: 20,
  },
});




