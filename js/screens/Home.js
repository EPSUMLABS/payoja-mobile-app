
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
import {
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab,Icon,Header,Left,Body,Title,Right,Item,Input
  } from 'native-base';
  const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
// import Share, {ShareSheet, Button} from 'react-native-share';
import urldetails from '../config/endpoints.json'
import Modal1 from "react-native-simple-modal";
import Modal2 from "react-native-simple-modal";
export default class Home extends Component {
  state={
    visible: false,
    user:[],
    open1:false,
    message:''
    // referal: this.props.navigation.state.params.referal,
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    console.log("back press")
    BackHandler.exitApp()
  };
  componentWillMount(){
    AsyncStorage.getItem('user')
    .then((value) => {
        if (value != null || value != undefined) {
            const values = JSON.parse(value)
            console.log('user available' + values.userid)
            console.log(values)
            this.setState({user:values,user_id:values.userid,referal:values.referal})
            console.log(this.state.referal)
            this.viewspin(values.userid)
            this.viewbids(values.userid)
        }
      })
  }
    //modal for show bid updation------------
    modalDidOpen2 = () => console.log("Modal did open");
    modalDidClose2 = () => {
      this.setState({ open2: false });
    };
    openModal2 = () => this.setState({ open2: true });
    closeModal2 = () => this.setState({ open2: false });
    //-------------------------------
  viewspin(user_id){
    var formData= new FormData();
    formData.append('user_id',user_id);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["creditspins"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      this.setState({accountspin:jsondata.accounts})
      console.log(this.state.accountspin)
    }
    else{
      this.setState({accountspin:jsondata.accounts})
      this.setState({open:false}) 
    }
  }).catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection.",
      [
        {text: 'Retry', onPress: () =>this.viewspin(this.state.user_id)},
      ],
      { cancelable: false }
    )
    });
  }
  viewbids(user_id){
    var formData= new FormData();
    formData.append('user_id',user_id);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["creditbids"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    // console.log(jsondata)
    // this.setState({open2:true,message:jsondata.message})
    if(jsondata.status=="success"){
    this.setState({open2:true,message:jsondata.message})
    }
   
  })
  }
  render() {
    let shareOptions = {
      title: "React Native",
      message: "referalid",
      url: "http://facebook.github.io/react-native/",
      text:"0132",
      subject: "Share Link" //  for email
    };
    return (
        <Container>
        <Content style={{backgroundColor:'#f5f5f5'}}>
        <Header style={{ backgroundColor: '#047BD5' }}>
          <Left>
          <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
          <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
          </TouchableOpacity>
          </Left>
          <Body><Title>PAYOJA</Title></Body>
          </Header>
          <StatusBar backgroundColor="#1565C0"/>
         <View style={{alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}}>
         <TouchableOpacity onPress={()=>this.props.navigation.navigate("Referandwin")} style={{marginTop:5,width:screenwidth-10,height:screenHeight/3.5,alignItems: 'center',backgroundColor:'#fff',justifyContent:'center',elevation:2 }}>
      <Image source={require('../assets/3.jpg')} style={{padding:20,height:screenHeight/4,width:screenwidth-40}}/>
      </TouchableOpacity>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate("Spinandwin")} style={{marginTop:5,width:screenwidth-10,height:screenHeight/3.5,alignItems: 'center',backgroundColor:'#fff',justifyContent:'center',elevation:2 }}>
      <Image source={require('../assets/1.jpg')} style={{padding:20,height:screenHeight/4,width:screenwidth-40}} />
      </TouchableOpacity>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate("Bidandwin")} style={{marginTop:5,width:screenwidth-10,height:screenHeight/3.5,alignItems: 'center',backgroundColor:'#fff',justifyContent:'center',elevation:2 }}>
      <Image source={require('../assets/2.jpg')} style={{padding:20,height:screenHeight/4,width:screenwidth-40}} />
      </TouchableOpacity>
           </View>
         </Content>
         <Modal2 offset={0} open={this.state.open2} modalDidOpen={this.modalDidOpen2} modalDidClose={this.modalDidClose2} style={{ backgroundColor: "#fff" }}>
      <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', }}>
                <Left></Left>
                <Right>
                    <TouchableOpacity   onPress={() => this.setState({open2:false})} >
                    <Text style={{ color: '#000', fontWeight: 'bold',fontSize:20, padding: 3, textAlign: 'center' }}>X</Text>
                    </TouchableOpacity>
                </Right>
            </View>
            <View style={{ flexDirection:'column' ,alignItems: 'center',justifyContent: 'center',marginTop:8 }}>
            <Image source={require('../assets/bid.png')} style={{padding:20,height:screenHeight/6,width:screenwidth/1.2}}/>
            <Text style={{ color: '#000', fontWeight: 'bold',fontSize:20, padding:10, textAlign: 'center' }}>{this.state.message}</Text>
            <TouchableOpacity  style={{marginTop:20,width:screenwidth/1.5,height: 40,backgroundColor: "#DE6F10",borderColor: 'rgba(255, 255, 255, 0.5)',
          alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}
          onPress={()=>this.props.navigation.navigate("Account")}>
            <Text style={{ color: "white", fontSize: 20 }}>Check Your Account</Text>
          </TouchableOpacity> 

           </View>
          </Modal2>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    marginTop: 20,
    marginBottom: 20,
  },
});



