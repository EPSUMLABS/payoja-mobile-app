import {DrawerNavigator,StackNavigator} from 'react-navigation';
import React, { Component } from 'react';
import { Platform,Dimensions } from 'react-native';
import Splash from '../screens/Splash';
import Login from '../screens/Login';
import Signup from '../screens/Signup';
import Home from '../screens/Home';
import Spinandwin from '../screens/Spinandwin';
import Bidandwin from '../screens/Bidandwin';
import Forgotpassword from '../screens/Forgotpassword';
import Account from '../screens/Account';
import Changepassword from '../screens/Changepassword';
import Sidebar from '../screens/Sidebar';
import Bidshop from '../screens/Bidshop';
import Logout from '../screens/Logout';
import Product from '../screens/Product';
import Win from '../screens/Win';
import Referandwin from '../screens/Referandwin';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const Drawer = ({
  // Home:{screen:Home},
  Spinandwin:{screen:Spinandwin},
  Home:{screen:Home},
  Bidandwin:{screen:Bidandwin},
  Account:{screen:Account},
  Bidshop:{screen:Bidshop},
  Referandwin:{screen:Referandwin},
  Win:{screen:Win},
  Product:{screen:Product}
});
const NavigationMenu = DrawerNavigator(
  Drawer,
   {
    // initialRouteName:'Home',
    drawerWidth: screenWidth - (Platform.OS === 'android' ? 56 : (screenWidth >414 ? 500 : 64)),
    contentComponent: props => <Sidebar {...props} routes={Drawer}/>,
    drawerPosition:'left',
  });
  const StackNav=StackNavigator(
    {
      Splash:{screen:Splash},
      Login:{screen:Login},
      Signup:{screen:Signup},
      Forgotpassword:{screen:Forgotpassword},
      Changepassword:{screen:Changepassword},
      Logout:{screen:Logout},
      NavigationMenu:{screen:NavigationMenu},
    },
  {headerMode:'none'});
  export default StackNav;
